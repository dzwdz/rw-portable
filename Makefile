PREFIX ?= /usr/local
EXEC_PREFIX ?= $(PREFIX)
BINDIR ?= $(EXEC_PREFIX)/bin
DATAROOTDIR ?= $(PREFIX)/share
MANDIR ?= $(DATAROOTDIR)/man

CFLAGS ?= -O2 -g
CFLAGS += -std=gnu99

.PHONY: all install clean distclean

all: rw

install: rw
	mkdir -p $(DESTDIR)$(BINDIR)
	cp rw $(DESTDIR)$(BINDIR)/rw
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp rw.1 $(DESTDIR)$(MANDIR)/man1/rw.1

clean:
	rm -f rw

distclean: clean
