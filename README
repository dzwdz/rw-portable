rw-portable
===========

Sortix rw is a program that reads blocks from the standard input and copies them
to the standard output until the end of the standard input. rw(1) is designed
to be a replacement for dd(1), providing only the the core blockwise I/O. It has
a standard command line interface with dashed options, useful and efficient
defaults, and is simpler because it doesn't support converting the input (e.g.
converting ASCII to/from EBCDIC).

This is the portable source code release of rw, which is developed as part of
the Sortix operating system. rw compiles and works on at least DragonFly, Haiku,
Hurd, Illumos, Linux, macOS, Midipix, Minix, NetBSD, OpenBSD, Sortix, and
Windows Subsystem for Linux.

rw is written in C99 using POSIX 2008 APIs, but also relies on features
that have been approved or suggested to the next POSIX revision, as well as
features that are available on the latest version of all modern POSIX systems.

Documentation
-------------

rw is documented in the rw.1 manual page in the mdoc(7) format.

Links
-----

For more information, please visit the official website:

  https://sortix.org/rw/

Building
--------

rw-portable comes with a simple portable makefile that can build and install rw.
For instance:

    make
    make install # as root

Will install rw at:

* `/usr/local/bin/rw` (`$BINDIR/rw`)
* `/usr/local/share/man/man1/rw.1` (`$MANDIR/man1/rw.1`)

Controlled by the following environment variables (see the Makefile for the full
list):

* `PREFIX`: The default prefix to install into (`/usr/local` by default).
* `BINDIR`: Where to install the executable (`$PREFIX/bin` by default).
* `MANDIR`: Where to install manual pages (`$PREFIX/share/man` by default).

Users of BSD and systems preferring `/usr/local/man` over `/usr/local/share/man`
will want to set the `MANDIR` environment variable to `/usr/local/man`, or the
manual page will not be installed at the right location:

    make MANDIR='$(PREFIX)/man' install

The compilation can be controlled with the `CC`, `CFLAGS`, and `LDFLAGS`
environment variables per the default Make C compilation rule. The `CPPFLAGS`
variable for C preprocessor options is also available on some systems, generally
those with GNU make, otherwise `CFLAGS` can be used.

The makefile should work on all the supported systems, but note the following
caveats:

* 32-bit Hurd, Illumos, and Linux systems may have a 32-bit off_t by default.
  To support large files on such systems, add `-D_FILE_OFFSET_BITS=64` to the
  `CPPFLAGS` environment variable during the build.
* Linux systems with an older glibc may have `clock_gettime` in `librt`, which
  is not linked in by default. To link rw on such systems, add `-lrt` to the
  `LDFLAGS` environment variable during the build.
* Illumos make does not support the `?=` and `+=` syntaxes that have been
  approved for the next POSIX release and are already available everywhere else.
  rw-portable can instead be built with GNU make.

Known Bugs
----------

rw is not yet able to determine the size of block devices on Illumos, Minix,
NetBSD, and OpenBSD.

rw does not handle tape archives specially, as dd does on some operating
systems.

rw does not support copying sparse files, a feature found on some operating
systems.

rw does not work around buggy operating systems that may report the preferred IO
block size `st_blksize` as 0 for some files.

License
-------

Copyright (c) 2016, 2017, 2018 Jonas 'Sortie' Termansen and contributors.

Sortix rw is free software licensed under the ISC license as described in the
LICENSE file.
